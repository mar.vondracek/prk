# PRK: compiler
Martin Vondráček

## U1: Můj vlastní jazyk

### Datové typy
- int32
- float
- boolean (0 a 1)

### Operace
- sčítání: +
- odčítání: -
- násobení: *
- modulo: %
- závorky: ()
- hranaté závorky: []

### Další funkce
- Umocňování: pow(number to be raised to a power, number that specifies a power)

### Originální vstup navíc:
- zakomentování části výrazu: //zakomentovaná část výrazu//

### Příklad:
[(a+a)*(a%b)-b]%pow(3,1)//+123//