# PRK: compiler
Martin Vondráček
## U2,3,4: ANTLR4: Gramatika, Lexikální, syntaktický analyzátor
```sh
/*
 * Parser Rules
 */
start:  (expr NEWLINE)*;
expr:   'pow(' expr ',' INT ')' # Pow
    |   expr ('*') expr# Mul
    |   expr ('%') expr # Mod
    |   expr ('+') expr # Add
    |   expr ('-') expr # Sub
    |   'pow(' expr ',' expr ')' # Pow
    |   '('expr')' # Par
    |   '['expr']' # Bra
    |   COMMENT # Com
    |   BOOLEAN  # Bln
    |   INT # Int
    |   FLOAT #Float
    ;

 /*
  * Lexer Rules
  */
WHITESPACE : (' ' | '\t') -> skip;
COMMENT  :  COMMENT_BORDER .*? COMMENT_BORDER -> skip;
BOOLEAN :[0-1];
INT     :   SIGN? NUMBER+ ;
FLOAT     : SIGN? NUMBER+ (PT NUMBER+)?;
NEWLINE : [\r\n]+ ;

fragment NUMBER : ('0' .. '9');
fragment PT  : '.';
fragment SIGN : ('+_' | '-_') ;
fragment COMMENT_BORDER : '//';
```

