import MyGrammar.MyGrammarBaseListener;
import MyGrammar.MyGrammarParser;
import org.antlr.v4.runtime.tree.ErrorNode;

import java.util.Stack;

public class AntlrMyGrammarListener extends MyGrammarBaseListener {
    private final Stack<Variable> stack = new Stack<>();

    public Stack<Variable> getStack(){
        return stack;
    }

    private String checkIfPrefix(String number){
        String ret = number;
        if(number.startsWith("-_")) {
            ret = "-" + number.substring(2);
        }else if (number.startsWith("+_")){
            ret = number.substring(2);
        }
        return ret;
    }

    private Variable getResultVariable(Variable right, Variable left){
        Variable ret;
        if(right.dataType == Variable.DataType.FLOAT || left.dataType == Variable.DataType.FLOAT) {
            ret = new Variable((float)0);
        }
        else ret = new Variable((int)0);
        return ret;
    }

    @Override public void exitFloat(MyGrammarParser.FloatContext ctx) {
        String num = checkIfPrefix(ctx.FLOAT().getText());
        float number = Float.parseFloat(num);
        this.stack.push(new Variable(number));

    }

    @Override public void exitBln(MyGrammarParser.BlnContext ctx) {
        String num = checkIfPrefix(ctx.BOOLEAN().getText());
        float number = Float.parseFloat(num);
        boolean bool;
        if(number >= 1) bool = true;
        else bool = false;
        this.stack.push(new Variable(bool));
    }

    @Override public void exitInt(MyGrammarParser.IntContext ctx) {
        String num = checkIfPrefix(ctx.INT().getText());
        int number = Integer.parseInt(num);
        this.stack.push(new Variable(number));
    }

    @Override public void exitAdd(MyGrammarParser.AddContext ctx) {
        Variable right = this.stack.pop();
        Variable left = this.stack.pop();
        Variable var = getResultVariable(right, left);
        var.value = left.value + right.value;
        this.stack.push(var);
    }

    @Override public void exitMod(MyGrammarParser.ModContext ctx) {
        Variable right = this.stack.pop();
        Variable left = this.stack.pop();
        Variable var = getResultVariable(right, left);
        var.value = left.value % right.value;
        this.stack.push(var);
    }

    @Override public void exitSub(MyGrammarParser.SubContext ctx) {
        Variable right = this.stack.pop();
        Variable left = this.stack.pop();
        Variable var = getResultVariable(right, left);
        var.value = left.value - right.value;
        this.stack.push(var);
    }

    @Override public void exitMul(MyGrammarParser.MulContext ctx) {
        Variable right = this.stack.pop();
        Variable left = this.stack.pop();
        Variable var = getResultVariable(right, left);
        var.value = left.value * right.value;
        this.stack.push(var);
    }

    @Override public void exitPow(MyGrammarParser.PowContext ctx) {
        Variable right = this.stack.pop();
        Variable left = this.stack.pop();
        Variable var = new Variable((float)Math.pow(left.value, right.value));
        this.stack.push(var);
    }

    @Override public void visitErrorNode(ErrorNode node) {
        System.out.println("Incorrect input");
        System.out.println(node.getText());
    }
}
