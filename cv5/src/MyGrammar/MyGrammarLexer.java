package MyGrammar;// Generated from MyGrammar.g4 by ANTLR 4.9.2
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MyGrammarLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.9.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, WHITESPACE=11, COMMENT=12, BOOLEAN=13, INT=14, FLOAT=15, NEWLINE=16;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	private static String[] makeRuleNames() {
		return new String[] {
			"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
			"T__9", "WHITESPACE", "COMMENT", "BOOLEAN", "INT", "FLOAT", "NEWLINE", 
			"NUMBER", "PT", "SIGN", "COMMENT_BORDER"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'pow('", "','", "')'", "'*'", "'%'", "'+'", "'-'", "'('", "'['", 
			"']'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, null, null, null, null, null, "WHITESPACE", 
			"COMMENT", "BOOLEAN", "INT", "FLOAT", "NEWLINE"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MyGrammarLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MyGrammar.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\22}\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5"+
		"\3\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13\3\f\3\f\3\f\3\f"+
		"\3\r\3\r\7\rI\n\r\f\r\16\rL\13\r\3\r\3\r\3\r\3\r\3\16\3\16\3\17\5\17U"+
		"\n\17\3\17\6\17X\n\17\r\17\16\17Y\3\20\5\20]\n\20\3\20\6\20`\n\20\r\20"+
		"\16\20a\3\20\3\20\6\20f\n\20\r\20\16\20g\5\20j\n\20\3\21\6\21m\n\21\r"+
		"\21\16\21n\3\22\3\22\3\23\3\23\3\24\3\24\3\24\3\24\5\24y\n\24\3\25\3\25"+
		"\3\25\3J\2\26\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16"+
		"\33\17\35\20\37\21!\22#\2%\2\'\2)\2\3\2\5\4\2\13\13\"\"\3\2\62\63\4\2"+
		"\f\f\17\17\2\u0081\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13"+
		"\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2"+
		"\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2"+
		"!\3\2\2\2\3+\3\2\2\2\5\60\3\2\2\2\7\62\3\2\2\2\t\64\3\2\2\2\13\66\3\2"+
		"\2\2\r8\3\2\2\2\17:\3\2\2\2\21<\3\2\2\2\23>\3\2\2\2\25@\3\2\2\2\27B\3"+
		"\2\2\2\31F\3\2\2\2\33Q\3\2\2\2\35T\3\2\2\2\37\\\3\2\2\2!l\3\2\2\2#p\3"+
		"\2\2\2%r\3\2\2\2\'x\3\2\2\2)z\3\2\2\2+,\7r\2\2,-\7q\2\2-.\7y\2\2./\7*"+
		"\2\2/\4\3\2\2\2\60\61\7.\2\2\61\6\3\2\2\2\62\63\7+\2\2\63\b\3\2\2\2\64"+
		"\65\7,\2\2\65\n\3\2\2\2\66\67\7\'\2\2\67\f\3\2\2\289\7-\2\29\16\3\2\2"+
		"\2:;\7/\2\2;\20\3\2\2\2<=\7*\2\2=\22\3\2\2\2>?\7]\2\2?\24\3\2\2\2@A\7"+
		"_\2\2A\26\3\2\2\2BC\t\2\2\2CD\3\2\2\2DE\b\f\2\2E\30\3\2\2\2FJ\5)\25\2"+
		"GI\13\2\2\2HG\3\2\2\2IL\3\2\2\2JK\3\2\2\2JH\3\2\2\2KM\3\2\2\2LJ\3\2\2"+
		"\2MN\5)\25\2NO\3\2\2\2OP\b\r\2\2P\32\3\2\2\2QR\t\3\2\2R\34\3\2\2\2SU\5"+
		"\'\24\2TS\3\2\2\2TU\3\2\2\2UW\3\2\2\2VX\5#\22\2WV\3\2\2\2XY\3\2\2\2YW"+
		"\3\2\2\2YZ\3\2\2\2Z\36\3\2\2\2[]\5\'\24\2\\[\3\2\2\2\\]\3\2\2\2]_\3\2"+
		"\2\2^`\5#\22\2_^\3\2\2\2`a\3\2\2\2a_\3\2\2\2ab\3\2\2\2bi\3\2\2\2ce\5%"+
		"\23\2df\5#\22\2ed\3\2\2\2fg\3\2\2\2ge\3\2\2\2gh\3\2\2\2hj\3\2\2\2ic\3"+
		"\2\2\2ij\3\2\2\2j \3\2\2\2km\t\4\2\2lk\3\2\2\2mn\3\2\2\2nl\3\2\2\2no\3"+
		"\2\2\2o\"\3\2\2\2pq\4\62;\2q$\3\2\2\2rs\7\60\2\2s&\3\2\2\2tu\7-\2\2uy"+
		"\7a\2\2vw\7/\2\2wy\7a\2\2xt\3\2\2\2xv\3\2\2\2y(\3\2\2\2z{\7\61\2\2{|\7"+
		"\61\2\2|*\3\2\2\2\f\2JTY\\aginx\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}