
public class Variable {
    float value;
    DataType dataType;

    public Variable(float value) {
        this.value = value;
        this.dataType = DataType.FLOAT;
    }

    public Variable(int value) {
        this.value = value;
        this.dataType = DataType.INT;
    }

    public Variable(boolean value) {
        if(value == true) this.value = 1;
        else this.value = 0;
        this.dataType = DataType.BOOLEAN;
    }

    enum DataType {
        BOOLEAN,
        INT,
        FLOAT
    }

}
