import MyGrammar.MyGrammarLexer;
import MyGrammar.MyGrammarParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Stack;

public class Main {

    public static void main(String[] args) throws IOException {

        InputStream inputStream = new FileInputStream(args[0]);
        MyGrammarLexer lexer = new MyGrammarLexer(new ANTLRInputStream(inputStream));

        // Get a list of matched tokens
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // Pass the tokens to the parser
        MyGrammarParser parser = new MyGrammarParser(tokens);

        // Specify our entry point
        MyGrammarParser.StartContext startContext = parser.start();

        // Walk it and attach our listener
        ParseTreeWalker walker = new ParseTreeWalker();
        AntlrMyGrammarListener listener = new AntlrMyGrammarListener();
        walker.walk(listener, startContext);

        Stack<Variable> stack = listener.getStack();
        for (Variable v: stack) {
            System.out.println(v.dataType + ": " + v.value);
        }
    }
}
